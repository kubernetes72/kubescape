# kubescape

## Setup the tool

See [kubescape Install section on Github](https://github.com/armosec/kubescape#install)

On MacOS:

```sh
brew tap armosec/kubescape
brew install kubescape
```

## Meet the tool

Prior to running any command, it may be valuable to create an account from <https://portal.armo.cloud>. Then get the account key and export it in an environnement variable.

```sh
export ARMO_ACCOUNT=025f1d3d-495f-4f1e-9d42-948ee74f9a8b
```

From now on, you should be able to submit reports into Armo portal. Try the following:

```sh
kubescape scan --enable-host-scan --submit --account=$ARMO_ACCOUNT
```

## Make use of a framework to define a Secutiry Gate

What is a framework ? In kubescape context, a framework is basically a set of controls that are checked against the cluster. There are some sets proposed by default:

* `nsa`, recommended by the [NSA](https://www.cncf.io/blog/2021/09/03/kubescape-the-first-open-source-tool-for-running-nsa-and-cisa-kubernetes-hardening-tests/)
* `mitre`, recommendend by Microsoft
* etc...

Get the currently supported frameworks by running the following command:

```sh
kubescape list frameworks
```

Let's choose one, `devopsbest` for example. Now run the scan towards it:

```sh
kubescape scan framework devopsbest --enable-host-scan --submit --account=$ARMO_ACCOUNT
```

## Mitigate scan results by filtering resources and defining a threshold

It may happen that some namespaces are out of the scope, so it become relevent to exclude them from the scan:

```sh
kubescape scan framework devopsbest --enable-host-scan --submit --account=$ARMO_ACCOUNT --exclude-namespaces kube-system,kube-public
```

By default the scan operation does not fail unless a threshold is defined. In order to fix that, specify it on the command line:

```sh
kubescape scan framework devopsbest --enable-host-scan --submit --account=$ARMO_ACCOUNT --exclude-namespaces kube-system,kube-public --fail-threshold 10
```

You may check the success of the command by running directly after:

```sh
echo $?
```

Any other result than zero means failure. It is handy when this command is used in the continuous integration system.

## Scanning images

The kubescape tool comes with the ability to scan images used in your cluster.

### Setup image scanning

```sh
helm repo add armo https://armosec.github.io/armo-helm/
helm upgrade --install armo armo/armo-cluster-components -n armo-system --create-namespace --set accountGuid=$ARMO_ACCOUNT --set clusterName=`kubectl config current-context`
```

**Attention:** check the deployemnt `armo-vuln-scan` is deployed properly, i met an oversized configuration supplied by helm.

After a while, the Armo portal fill up with images known as vulnerable.

## Checking RBAC

Last but not least, kubescape helps with RBAC analysis. Run the following command:

```sh
kubescape submit rbac --account $ARMO_ACCOUNT
```

## Decide what matter for your Security Gate

With kubescape, there is no such thing as *Security Gate*, there are only frameworks, as described previously. The defaults are great but don't necessarly match your need globaly or per environnement.

This is where the concept of Security Gate should take place, imho. It would describe what you expect as controls to be checked regarding how the security of the targeted cluster should be setup.

As a first rule of thumbs, environnement should be considered as a discriminant: at least the Production environment as it is the goal to achieve, and from that one the Developement environment that may be a subset of the it.

Then, as a second rule of thumbs, the Production environnement should implemenent a reasonnable set of controls regarding the best practices. For example, the nsa framework gather a comprehensive and valuable list of recommendations that may serve as a baseline. Thus, they should be reviewed to decide whether or not it should be kept in your usecase.

This leads to the third rule of thumbs which would be describe explicitely any exception your security gate assumes. It should be project dependent and kept safe in source code management (like a git repository). This is accomplished at least by two approaches:

* Define your own framework, which assume either you have an Armo account, either you have redefined it locally and supply it for each scan
* Use a default framework like `nsa` with a locally defined set of exception

IMHO, the second approach is more relevent as it allows a better understanding of what is voluntarly ignored and why.

Let's dive into an example. Starts with the creation of a folder structure:

```sh
mkdir -p ./exceptions
```

The exception to add concern [C-0066 - Secret/ETCD encryption enabled](https://hub.armosec.io/docs/c-0066). As the cluster is not cloud managed, this control is irrelevant. Add a JSON file at `exceptions/production.json` with the following content:

```json
[
    {
        "name": "exclude-secret-etcd-encryption-enabled",
        "policyType": "postureExceptionPolicy",
        "actions": [
            "alertOnly"
        ],
        "resources": [
            {
                "designatorType": "Attributes",
                "attributes": {
                    "kind": ".*"
                }
            }
        ],
        "posturePolicies": [
            {
                "controlId": "C-0066" 
            }
        ]
    }
]
```

Then run the following command:

```sh
kubescape scan framework nsa --enable-host-scan --submit --account=$ARMO_ACCOUNT --exceptions ./exceptions/production.json
```

You should be able to observe that `C-0066` does not impact risk score anymore.

Let's go further and add a new exception for the Development environement. As explained before, it would be a superset of the Production environment. Duplicate `exceptions/production.json` into `exceptions/development.json`, then add the following exception:

```json
{
    "name": "exclude-resource-policies",
    "policyType": "postureExceptionPolicy",
    "actions": [
        "alertOnly"
    ],
    "resources": [
        {
            "designatorType": "Attributes",
            "attributes": {
                "kind": ".*"
            }
        }
    ],
    "posturePolicies": [
        {
            "controlId": "C-0009"
        }
    ]
}
```

It basically assumes that resources doesn't have to be considered in the Development environment.

Then run the following command:

```sh
kubescape scan framework nsa --enable-host-scan --submit --account=$ARMO_ACCOUNT --exceptions ./exceptions/development.json
```

Hurray! From now on, scans can performed with a set of documented exceptions.

Much more can be achieved, have a look at [the official documentation on exceptions](https://hub.armosec.io/docs/exceptions).

## Resources

* [How To Secure Kubernetes Clusters With Kubescape And Armo](https://www.youtube.com/watch?v=ZATGiDIDBQk)
* [The official documentation](https://hub.armosec.io/docs)
